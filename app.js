// モジュールをロード
var express = require('express');  
var app = express();  
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');


//Expressのテンプレートについてくるので
//一応ロードする。
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//faviconランダム表示処理 準備
var ary_favicon = ["broom.ico", "cthu.ico", "doc.ico", "favicon.ico", "goko.ico", "kanoke_huta.ico", "kanoke_naka.ico", "komorin.ico", "koumori.ico", "kuma.ico", "kumo.ico", "neko.ico", "neko_nuru.ico", "obake.ico", "obake2.ico", "obake3.ico", "obakechan.ico", "obakechan2.ico", "pamp.ico", "present.ico", "pumpkin.ico", "pumpkin2.ico", "skeleton.ico", "skeleton2.ico", "tare_orange.ico", "tare_pp.ico", "usagi.ico", "wax.ico", "wax_fire.ico" ];
var rand_favicon = "";

//path.join()は引数に指定されたファイルを"/"で区切りパス情報にする
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico' )));
app.set('views', path.join(__dirname, 'views'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//public直下のリソースを使用する際に、
//サーバー側、クライアント側の両方の記述が楽になる
app.use(express.static(path.join(__dirname, 'public')));

//-------------クライアント用ページ-------------
app.get('/', function(req, res,next) { 
    //client.htmlを送る
    res.sendFile(__dirname + '/views/client.html');
    
    //ランダムでfaviconを変える処理
    //public直下のfavicon.icoの画像は変わっているようだが、それが機能しているのかはわからない。
    //時々faviconは変わるがサーバーを再起動させたためなのか、クライアントのキャッシュがあるためなのか、原因はわからない。
    rand_favicon = ary_favicon[Math.floor(Math.random()*(ary_favicon.length - 1))];
    fs.createReadStream( __dirname + '/public/image/16x16/' + rand_favicon ).pipe(fs.createWriteStream( __dirname + '/public/favicon.ico'));
    console.log('rand_favicon ' + rand_favicon);
});
//------------------------------------------------

//-------------プロジェクター表示用ページ-------------
//gitが作ったのハッシュ値をページのパスとしてみた。
//ここにアクセスできる人はいないだろう。
app.get('/e344c815e6c3dbd2067c6f8ee6a27b83a8c1680c', function(req, res,next) { 
    //projector.htmlを送る
    res.sendFile(__dirname + '/views/projector.html');
});
//--------------------------------------------------------

//-------------Socket通信-------------
//アクセスカウンター用変数
var accCounter = 0;
io.on('connection', function(socket) {  
    
    console.log('rand_favicon ' + rand_favicon);
    
    //現在接続中のクライアント数を送る
    io.sockets.emit("current_connection", socket.client.conn.server.clientsCount);
    
    //クライアントからの接続があったらアクセスカウンター用変数を1プラス
    accCounter++;
    
    //だれがどこから来たかを出力するためのログ
    //(出力されたものはサーバーのコマンドで記録する)
    var header = socket.handshake;
    console.log('time: ' + header.time);
    console.log('IP_address: ' + header.address);
    console.log('user-agent: ' + header.headers['user-agent']);
    console.log('referer: ' + header.headers.referer + "\n");
    console.log("increased access_counter: " + accCounter);

    //アクセスカウンターのデータを送る
    socket.emit('acc', accCounter);
    
    //クライアント用ページから選択された位置を取得して
    //プロジェクター用ページに転送する
    socket.on('selected_col_num', function(data) {
        io.sockets.emit('selected_col_num_from_server', data); 
    });

    socket.on('selected_pic_name', function(data) {
       socket.broadcast.emit('selected_pic_name_from_server', data); 
    });

    //クライアントが接続を切断したら
    socket.on('disconnect', function () {
        io.sockets.emit("current_connection", socket.client.conn.server.clientsCount);
    });
    
});
//----------------------------------------

//--------------Error Handler-------------
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

//------ production error handler-----
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
//---------------------------------------

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
