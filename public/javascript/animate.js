function honzawa(){
	$(function(){
		if(!click_flag_kanoke==false){
    		click_flag_kanoke = false;
    		$("#projection").append("<img class='onwrite' src='img/kanoke_huta.png'>").append("<img src='img/kanoke_naka.png'>");
    		var rand_kanoke = Math.floor(Math.random() * 700) +1;
    		$("img[src*=kanoke]").css('left',rand_kanoke);//存在しないオブジェクトに対して変更きかない
			$("img[src*=kanoke_huta]").fadeIn(100);
			$("img[src*=kanoke_naka]").fadeIn(500,kanoke1);
		}else{
			return false;
		}
	});
	function kanoke1(){
		$("img[src*=kanoke_huta]").animate({marginLeft:-50},{duration: 700, easing: 'easeInQuad'}).delay(600).animate({marginLeft:0},{complete:kanokeend});
	}
	function kanokeend(){
		$("img[src*=kanoke_naka]").remove();
		$("img[src*=kanoke_huta]").fadeOut(1000,function(){
			$(this).remove();
			click_flag_kanoke = true;
		});
	}
}
function witch(){
	/*魔女っ娘*/
	$(function(){
		if(!click_flag_witch==false){
    		click_flag_witch = false;
    		$("#projection").append("<img src='img/witch.png'>");
    		var rand_witch = Math.floor(Math.random() * 300) +1;
    		$("img[src*=witch]").css('top',rand_witch).fadeIn(10,witch);
		}else{
			return false;
		}
	});
	function witch(){
		$("img[src*=witch]").animate({left:-300},{duration: 5000, easing: 'easeOutCubic',complete:witchend});
	}
	function witchend(){
		$("img[src*=witch]").fadeOut(100,function(){
			$(this).remove();
			click_flag_witch = true;
		});
	}
}

function broom(){
	$(function(){
		if(!click_flag_broom==false){
    		click_flag_broom = false;
    		$("#projection").append("<img src='img/broom.png'>");
    		var rand_broom = Math.floor(Math.random() * 200) +1;
    		$("img[src*=broom]").css('top',rand_broom).fadeIn(10,broom);
		}else{
			return false;
		}
	});
	function broom(){
		$("img[src*=broom]").addClass("goLeft").animate({top: '+=200'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '-=200'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '+=200'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '-=200'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '+=200'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '-=200'},{duration: 1000,easing: 'easeInOutQuad',complete:broomend});
	}
	function broomend(){
		$("img[src*=broom]").fadeOut(400,function(){
			$(this).remove();
			click_flag_broom = true;//右向き左向きで繰り返しが違うなぜかわからｎ
		});
	}
}

function komorin(){
	$(function(){
		if(!click_flag_komorin==false){
    		click_flag_komorin = false;
    		$("#projection").append("<img src='img/komorin.png'>");
    		var rand_komorin = Math.floor(Math.random() * 200) +1;
    		$("img[src*=komorin]").css('top',rand_komorin).fadeIn(10,komorin);
		}else{
			return false;
		}
	});
	function komorin(){
		$("img[src*=komorin]").addClass("goRight").animate({top: '+=300'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '-=300'},{duration: 1000,easing: 'easeInOutQuad'})
		.animate({top: '+=300'},{duration: 1000,easing: 'easeInOutQuad',complete:komorinend});
	}
	function komorinend(){
		$("img[src*=komorin]").fadeOut(400,function(){
			$(this).remove();
			click_flag_komorin = true;
		});
	}
}
function obake2(){
	$(function(){
		if(!click_flag_obake2==false){
    		click_flag_obake2 = false;
    		$("#projection").append("<div id='obake2'><img src='img/obake2.png'><img src='img/flash.png'></div>");
    		var rand_obake2_w = Math.floor(Math.random() * 700) +1;
    		var rand_obake2_h = Math.floor(Math.random() * 400) +1;
    		$("#obake2").css({'top':rand_obake2_h,'left':rand_obake2_w}).fadeIn(200,obake2);
		}else{
			return false;
		}
	});
	function obake2(){
		$("img[src*=flash]").fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100,obake2end);
	}
	function obake2end(){
		$("#obake2").fadeOut(100,function(){
			$(this).remove();
			click_flag_obake2 = true;
		});
	}
}
function doc(){
	$(function(){
		if(!click_flag_doc==false){
    		click_flag_doc = false;
    		$("#projection").append("<img src='img/doc.png'>");
    		var rand_doc_w = Math.floor(Math.random() * 700) +1;
    		var rand_doc_h = Math.floor(Math.random() * 400) +1;
    		$("img[src*=doc]").css({'top':rand_doc_h,'left':rand_doc_w}).fadeIn(300,doc);
		}else{
			return false;
		}
	});
	function doc(){
		$("img[src*=doc]").addClass("swing").delay(1000).fadeOut(400,docend);
	}
	function docend(){
		$("img[src*=doc]").remove();
		click_flag_doc = true;
	}
}
function goko(){
	$(function(){
		if(!click_flag_goko==false){
    		click_flag_goko = false;
    		$("#projection").append("<img src='img/goko.png'>");
    		var rand_goko_w = Math.floor(Math.random() * 700) +1;
    		var rand_goko_h = Math.floor(Math.random() * 10) +400;
    		$("img[src*=goko]").css({'top':rand_goko_h,'left':rand_goko_w}).fadeIn(300,goko);
		}else{
			return false;
		}
	});
	function goko(){
		$("img[src*=goko]").delay(500).fadeOut(300,gokoend);
	}
	function gokoend(){
		$("img[src*=goko]").remove();
		click_flag_goko = true;
	}
}
function obakechan2(){
	$(function(){
		if(!click_flag_obakechan2==false){
    		click_flag_obakechan2 = false;
    		$("#projection").append("<div class='circle'><div class='common top'><div class='content'><img src='img/obakechan2.png'></div></div></div>");
    		var rand_obakechan2_w = Math.floor(Math.random() * 200) +300;
    		var rand_obakechan2_h = Math.floor(Math.random() * 200) +1;
    		$(".circle").css({'top':rand_obakechan2_h,'left':rand_obakechan2_w}).fadeIn(1000,obakechan2);
		}else{
			return false;
		}
	});
	function obakechan2(){
		$(".circle").delay(1000).fadeOut(500,obakechan2end);
	}
	function obakechan2end(){
		$(".circle").remove();
		click_flag_obakechan2 = true;
	}
}

function obake3(){
	$(function(){
		if(!click_flag_obake3==false){
    		click_flag_obake3 = false;
    		$("#projection").append("<img class='xyura' src='img/obake3.png'>");
    		var rand_obake3_w = Math.floor(Math.random() * 700) +1;
    		$("img[src*=obake3]").css({'left':rand_obake3_w}).fadeIn(20,obake31);
		}else{
			return false;
		}
	});
	function obake31(){
		$("img[src*=obake3]").animate({top:300},{duration: 3000, easing: 'easeOutCubic'}).delay(200).fadeOut(500,obake3end);
	}
	function obake3end(){
		$("img[src*=obake3]").remove();
		click_flag_obake3 = true;
	};
}
function obake1(){
	$(function(){
		if(!click_flag_obake1==false){
   			click_flag_obake1 = false;
    		$("#projection").append("<img class='xyura' src='img/obake1.png'>");
    		var rand_obake1_w = Math.floor(Math.random() * 700) +1;
    		var rand_obake1_h = Math.floor(Math.random() * 400) +1;
    		$("img[src*=obake1]").css({'top':rand_obake1_h,'left':rand_obake1_w}).fadeIn(300,obake11);
		}else{
			return false;
		}
	});
	function obake11(){
		$("img[src*=obake1]").delay(2000).fadeOut(300,obake1end);
	}
	function obake1end(){
		$("img[src*=obake1]").remove();
		click_flag_obake1 = true;
	};
}
function pumpkin1(){
	$(function(){
		if(!click_flag_pumpkin1==false){
    		click_flag_pumpkin1 = false;
    		$("#projection").append("<img class='xyura' src='img/pumpkin1.png'>");
    		var rand_pumpkin1_w = Math.floor(Math.random() * 700) +1;
    		var rand_pumpkin1_h = Math.floor(Math.random() * 400) +1;
    		$("img[src*=pumpkin1]").css({'top':rand_pumpkin1_h,'left':rand_pumpkin1_w}).fadeIn(300,pumpkin11);
		}else{
			return false;
		}
	});
	function pumpkin11(){
		$("img[src*=pumpkin1]").fadeOut(300).fadeIn(300).delay(100).fadeOut(300,pumpkin1end);
	}
	function pumpkin1end(){
		$("img[src*=pumpkin1]").remove();
		click_flag_pumpkin1 = true;
	};
}
function pumpkin2(){
	$(function(){
		if(!click_flag_pumpkin2==false){
    		click_flag_pumpkin2 = false;
    		$("#projection").append("<img class='ygata' src='img/pumpkin2.png'>");
    		var rand_pumpkin2_w = Math.floor(Math.random() * 700) +1;
    		var rand_pumpkin2_h = Math.floor(Math.random() * 1) +350;
    		$("img[src*=pumpkin2]").css({'top':rand_pumpkin2_h,'left':rand_pumpkin2_w}).fadeIn(400,pumpkin21);
		}else{
			return false;
		}
	});
	function pumpkin21(){
		$("img[src*=pumpkin2]").animate({top:-300},{duration: 2000, easing: 'easeInBounce',complete:pumpkin2end});
	}
	function pumpkin2end(){
		$("img[src*=pumpkin2]").remove();
		click_flag_pumpkin2 = true;
	};
}
function pumpkin3(){
	$(function(){
		if(!click_flag_pumpkin3==false){
    		click_flag_pumpkin3 = false;
    		$("#projection").append("<img src='img/pumpkin3.png'>");
    		var rand_pumpkin3_w = Math.floor(Math.random() * 400) +1;
    		$("img[src*=pumpkin3]").css({'left':rand_pumpkin3_w}).fadeIn(20,pumpkin31);
		}else{
			return false;
		}
	});
	function pumpkin31(){
		$("img[src*=pumpkin3]").animate({top:500},{duration: 1000, easing: 'easeInExpo'}).fadeOut(100,pumpkin3end);
	}
	function pumpkin3end(){
		$("img[src*=pumpkin3]").remove();
		click_flag_pumpkin3 = true;
	};
}
function wax_fire(){
	$(function(){
		if(!click_flag_wax_fire==false){
    		click_flag_wax_fire = false;
    		$("#projection").append("<img class='tenmetu' src='img/wax_fire.png'>");
    		var rand_wax_fire_w = Math.floor(Math.random() * 400) +1;
    		$("img[src*=wax_fire]").css({'left':rand_wax_fire_w}).fadeIn(20,wax_fire1);
		}else{
			return false;
		}
	});
	function wax_fire1(){
		$("img[src*=wax_fire]").delay(3000).fadeOut(300,wax_fireend);
	}
	function wax_fireend(){
		$("img[src*=wax_fire]").remove();
		click_flag_wax_fire = true;
	};
}
function kumo(){
	$(function(){
		if(!click_flag_kumo==false){
    		click_flag_kumo = false;
    		$("#projection").append("<img src='img/kumo.png'>");
    		var rand_kumo_w = Math.floor(Math.random() * 400) +1;
    		$("img[src*=kumo]").css({'left':rand_kumo_w}).fadeIn(20,kumo1);
		}else{
			return false;
		}
	});
	function kumo1(){
		$("img[src*=kumo]").animate({top:0},{duration: 2000, easing: 'easeOutQuint'}).delay(200).animate({top:-300},{duration: 2000, easing: 'easeInCubic',complete:kumoend});
	}
	function kumoend(){
		$("img[src*=kumo]").remove();
		click_flag_kumo = true;
	};
}
function kuma(){
	$(function(){
		if(!click_flag_kuma==false){
    		click_flag_kuma = false;
    		$("#projection").append("<img class='rotate' src='img/kuma.png'>");
    		$("img[src*=kuma]").fadeIn(20,kuma1);
		}else{
			return false;
		}
	});
	function kuma1(){
		$("img[src*=kuma]").animate({right:-100},{duration: 1500, easing: 'easeOutSine',complete:kuma2});
		//rotateと一緒に移動するとバグ
	}
	function kuma2(){
		$("img[src*=kuma]").animate({right:-300},{duration: 500, easing: 'easeOutSine',complete:kumaend});
	}
	function kumaend(){
		$("img[src*=kuma]").remove();
		click_flag_kuma = true;
	};
}
function koumosuke(){
	$(function(){
		if(!click_flag_koumosuke==false){
    		click_flag_koumosuke = false;
    		$("#projection").append("<img src='img/koumosuke.png'>");
    		var rand_koumosuke_w = Math.floor(Math.random() * 700) +1;
    		var rand_koumosuke_h = Math.floor(Math.random() * 100) +300;
    		$("img[src*=koumosuke]").css({'top':rand_koumosuke_h,'left':rand_koumosuke_w}).fadeIn(10,koumosuke1);
		}else{
			return false;
		}
	});
	function koumosuke1(){
		$("img[src*=koumosuke]").animate({top:-1000},{duration: 4000, easing: 'easeInOutBounce',complete:koumosukeend});
	}
	function koumosukeend(){
		$("img[src*=koumosuke]").remove();
		click_flag_koumosuke = true;
	};
}
function neko_kama(){
	$(function(){
		if(!click_flag_neko_kama==false){
    		click_flag_neko_kama = false;
    		$("#projection").append("<img src='img/neko_nuru.png'><img src='img/neko_kama.png'>");
    		$("img[src*=neko_nuru]").addClass("nuru").fadeIn(20);
    		$("img[src*=neko_kama").addClass("kama").fadeIn(20).delay(4000).fadeOut(20,neko_kama1);
		}else{
			return false;
		}
	});
	function neko_kama1(){
		$("#projection").append("<img src='img/blood.png'>").animate({color:'black'},{duration: 200, easing: 'easeInOutBounce',complete:neko_kamaend});
	}
	function neko_kamaend(){
		$("img[src*=neko],img[src*=blood]").remove();
		click_flag_neko_kama = true;
	};
}
function cthu(){
	$(function(){
		if(!click_flag_cthu==false){
    		click_flag_cthu = false;
    		$("#projection").append("<img src='img/black.png'>").append("<img src='img/cthu.png'>");
    		$("img[src*=black]").fadeIn(1000,cthu1);
		}else{
			return false;
		}
	});
	function cthu1(){
		$("img[src*=cthu]").fadeIn(10);
		$("img[src*=black]").animate({opacity:0},{duration: 1000, easing: 'linear',complete:cthu2});
	}
	function cthu2(){
		$("img[src*=black]").animate({opacity:1},{duration: 1000, easing: 'linear',complete:cthuend});
	}
	function cthuend(){
		$("img[src*=cthu]").remove();
		$("img[src*=black]").fadeOut(1000,function(){
			$(this).remove();
			click_flag_cthu = true;
		});
	};
}
function skeleton2(){
	$(function(){
		if(!click_flag_skeleton2==false){
    		click_flag_skeleton2 = false;
    		$("#projection").append("<img src='img/skeleton2.png'>");
    		var rand_skeleton2_w = Math.floor(Math.random() * 400) +1;
    		$("img[src*=skeleton2]").css({'left':rand_skeleton2_w}).fadeIn(20,skeleton21);
		}else{
			return false;
		}
	});
	function skeleton21(){
		$("img[src*=skeleton2]").animate({bottom:-100},{duration: 2000, easing: 'swing'}).delay(200).animate({bottom:-300},{duration: 2000, easing: 'swing',complete:skeleton2end});
	}
	function skeleton2end(){
		$("img[src*=skeleton2]").remove();
		click_flag_skeleton2 = true;
	};
}
function usagi(){
	$(function(){
		if(!click_flag_usagi==false){
    		click_flag_usagi = false;
    		$("#projection").append("<img src='img/usagi.png'>");
    		$("img[src*=usagi]").fadeIn(10,usagi);
		}else{
			return false;
		}
	});
	function usagi(){
		$("img[src*=usagi]").animate({left:-300},{duration: 10000, easing: 'linear',complete:usagiend});
	}
	function usagiend(){
		$("img[src*=usagi]").fadeOut(100,function(){
			$(this).remove();
			click_flag_usagi = true;
		});
	}
}
function haikei1(){
	$(function(){
		if(!click_flag_haikei==false){
    		click_flag_haikei = false;
    		$("img[src*=left]").animate({right:775},{duration: 1000, easing: 'linear',queue: false});
			$("img[src*=right]").animate({left:775},{duration: 1000, easing: 'linear',queue: false,complete:haikei1end});
		}else{
			return false;
		}
	});
	function haikei1end(){
		$("#projection").css({'background-image':'url(img/haikei1.png)'});//htmlからみて
		$("img[src*=left]").animate({right:1300},{duration: 1000, easing: 'linear',queue: false});
		$("img[src*=right]").animate({left:1300},{duration: 1000, easing: 'linear',queue: false,complete:haikeiend});
	}
	function haikeiend(){
		click_flag_haikei = true;
	}
}
function haikei2(){
	$(function(){
		if(!click_flag_haikei==false){
    		click_flag_haikei = false;
    		$("img[src*=left]").animate({right:775},{duration: 1000, easing: 'linear',queue: false});
			$("img[src*=right]").animate({left:775},{duration: 1000, easing: 'linear',queue: false,complete:haikei2end});
		}else{
			return false;
		}
	});
	function haikei2end(){
		$("#projection").css({'background-image':'url(img/haikei2.png)'});//htmlからみて
		$("img[src*=left]").animate({right:1300},{duration: 1000, easing: 'linear',queue: false});
		$("img[src*=right]").animate({left:1300},{duration: 1000, easing: 'linear',queue: false,complete:haikeiend});
	}
}
function haikei3(){
	$(function(){
		if(!click_flag_haikei==false){
    		click_flag_haikei = false;
    		$("img[src*=left]").animate({right:775},{duration: 1000, easing: 'linear',queue: false});
			$("img[src*=right]").animate({left:775},{duration: 1000, easing: 'linear',queue: false,complete:haikei3end});
		}else{
			return false;
		}
	});
	function haikei3end(){
		$("#projection").css({'background-image':'url(img/haikei3.jpg)'});//htmlからみて
		$("img[src*=left]").animate({right:1300},{duration: 1000, easing: 'linear',queue: false});
		$("img[src*=right]").animate({left:1300},{duration: 1000, easing: 'linear',queue: false,complete:haikeiend});
	}
}